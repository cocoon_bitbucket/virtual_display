import time
from pyvirtualdisplay import Display
from selenium import webdriver
from selenium.webdriver.common.keys import Keys


display = Display(visible=0, size=(1024, 768))
display.start()

driver = webdriver.Firefox()
actions = webdriver.ActionChains(driver)
driver.get('http://www.python.org')
time.sleep(5) # sleep for 5 seconds

print "check page title is Python"
assert "Python" in driver.title
elem = driver.find_element_by_name("q")
elem.clear()

print ("send pycon to page")
elem.send_keys("pycon")
elem.send_keys(Keys.RETURN)

print( "check No results found")
assert "No results found." not in driver.page_source

driver.close()
display.stop()

