virtual_display
===============


be able to run selenium webdriver without a physical display on debian



this repository contains a test_virtual_display.py to be run in a vagrant vm ( debian jessie )


vm installation:

apt
---

* lxde         ( a light gui )
* iceweasel    ( a compatible firefox browser
* xvfb         ( a virtual display )

pip
---

* selenium
* pyvirtualdisplay


the test ( test_virtual_display.py)
 
* creates a virtual display with pyvirtualdisplay
* creates a selenium web driver for firefox (iceweasel)
* load a web page (python.org)
* performs some controls on this page

the test must pass without any exceptions




run the test
============

    vagrant up
    vagrant ssh
    cd /usr/local/lib/virtual_display
    python test_virtual_display.py
